// We hate you !

var pseudos = [];
var whitelist = ['Sevenn', 'Peshmelba', 'OrangeCape', 'Wddysr', 'Sakiru', 'ThePonyIsASpy'];
var TextArea = document.querySelector('#mainEdit textarea');

//Muter
function isDashMute(message) {
    "use strict";
    var mute = /^\/mute /i;
    return mute.test(message);
}

function isDashUnmute(message) {
    "use strict";
    var mute = /^\/unmute /i;
    return mute.test(message);
}

function isDashMutelist(message) {
    "use strict";
    return message === "/mutelist";
}

function setback(unpseudo) {
    "use strict";
    var messages = document.querySelectorAll('#receivepb li .nickpb'),
        online = document.querySelectorAll('#listeConnecte li'),
        i, a;

    for (i = 0; i < messages.length; ++i) {
        if (messages[i].textContent === (unpseudo + " : ")) {
            messages[i].parentNode.parentNode.style.display = "block"; // Et on affiche le les div caché
        } else if (messages[i].textContent !== (unpseudo + " ")) {
            messages[i].parentNode.style.display = "block"; // Et on affiche le les div caché
        }
    }

    for (a = 0; a < online.length; a++) {
        if (online[a].lastChild.textContent === unpseudo) {
            online[a].firstChild.remove(); //suppression du <span> ajouter avec la function mute
        }
    }
}

function scanf() {
    "use strict";
    if (event.keyCode === 13) {
        if (isDashMute(scanf.text)) {
            var pseudo = scanf.text, mute = /^\/mute /i;
            mute.test(pseudo);
            pseudo = RegExp.rightContext;
            if (whitelist.indexOf(pseudo) !== -1) {
                alert("Vilaine ! Tu mute pas cette personne !");
            } else if (pseudos.indexOf(pseudo) === -1 && confirm('Voulez-vous mute ' + pseudo + ' ?')) {
                pseudos.push(pseudo);
                // Affichage d'un /me dans la CB
                /* TextArea.value += "/me a mute " + pseudo;
                send.click(); */
            } else {
                alert('Cette personne est déjà mute D:');
            }
        }

        if (isDashUnmute(scanf.text)) {
            var unpseudo = scanf.text,
                unmute = /^\/unmute /i;

            unmute.test(unpseudo);
            unpseudo = RegExp.rightContext;
            setback(unpseudo);
            pseudos.splice(pseudos.indexOf(unpseudo), 1); // Retire le pseudo de l'array pseudos
        }

        if (isDashMutelist(scanf.text)) {
            alert("Liste des Personnes que vous mutez :\n" + pseudos.join('\n'));
        }
    } else {
        scanf.text = TextArea.value;
    }
}

function mute() {
    "use strict";
    var messages = document.querySelectorAll('#receivepb li .nickpb'),
        online = document.querySelectorAll('#listeConnecte li'),
        text = document.createTextNode('[MUTED] '),
        muted = document.createElement('span'),
        j, i, a; // creation des variable de boucle

    muted.style.color = "red";
    muted.appendChild(text);

    for (j = 0; j < pseudos.length; j++) {
        for (i = 0; i < messages.length; i++) {
            if (messages[i].textContent === (pseudos[j] + " : ")) {
                messages[i].parentNode.parentNode.style.display = "none";
            } else if (messages[i].textContent === (pseudos[j] + " ")) {
                messages[i].parentNode.style.display = "none";
            }
        }
        for (a = 0; a < online.length; a++) {
            if (online[a].textContent === pseudos[j]) {
                online[a].insertBefore(muted, online[a].firstChild);
            }
        }
    }
}

//var send = document.getElementById('peEnvoyer'); // Utilisé send pour simulé l'appuis sur le bouton envoie
TextArea.addEventListener('keyup', scanf);

setInterval(mute, 100);